using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2

{
    class Program
    {
        static void Main(string[] args)
        {
            DiceRoller diceRoller = new DiceRoller();
            Random randomGenerator = new Random(); //zadatak 2

            for (int i = 0; i < 20; i++)
            {
                //diceRoller.InsertDie(new Die(6));                     //1. zadatak
                diceRoller.InsertDie(new Die(6, randomGenerator));      //2. zadatak
            }
            diceRoller.RollAllDice();

            IList<int> resultsOfRolling = diceRoller.GetRollingResults();
            foreach (int result in resultsOfRolling)
            {
                Console.WriteLine(result);
            }
        }
    }
}
